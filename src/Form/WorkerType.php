<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class WorkerType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      ->add('name', TextType::class, array(
        'label' => 'Nazwa',
				'attr' => array(
					'class' => 'form-control'
				),
      ))
      ->add('status', CheckboxType::class, array(
        'required' => false,
        'label' => 'Status',
        'attr' => array(
          'class' => 'custom-control-input',
        ),
      ))
			->add('save', SubmitType::class, ['label' => 'Zapisz', 'attr' => array('class' => 'btn btn-success btn-sm btn-block')]);
  }
}