<?php

namespace App\Repository;

use App\Entity\Worker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WorkerRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Worker::class);
  }

  public function findAllQuery($agent)
  {
    $qb = $this->createQueryBuilder('w')
			->where('w.agents= ' . $agent->getId())
			->orderBy('w.id', 'DESC');

    return $qb->getQuery();
  }

	public function findForChart()
	{
		$qb = $this->createQueryBuilder('w')
			->addSelect('count(w.id)')
			->andwhere('w.status = true')
			->orderBy('count(w.id)', 'DESC')
			->leftJoin('w.agents', 'a')
			->groupBy('a.id')
			->setMaxResults( 3 );

		return $qb->getQuery()->getResult();
	}
}