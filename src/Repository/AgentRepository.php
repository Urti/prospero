<?php

namespace App\Repository;

use App\Entity\Agent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AgentRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Agent::class);
  }

  public function findAllQuery($filterData = [])
  {
    $qb = $this->createQueryBuilder('a')
      ->orderBy('a.id', 'DESC');

    return $qb->getQuery();
  }
}