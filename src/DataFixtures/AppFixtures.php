<?php

namespace App\DataFixtures;

use App\Entity\Agent;
use App\Entity\Worker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
    	for ($i=0; $i < 5; $i++){
				$newAgent = new Agent();
				$newAgent->setName('Agent '.$i);
				$newAgent->setStatus(true);
				$newAgent->setWorkersCount(0);
				$manager->persist($newAgent);
				for ($a=0; $a < rand(1,5); $a++) {
					$newWorker = new Worker();
					$newWorker->setName('Pracownik ' . $a);
					$newWorker->setStatus(true);
					$newWorker->setAgent($newAgent);
					$newWorker->setAgentIp(ip2long('127.0.0.1'));
					$newAgent->setWorkersCount($newAgent->getWorkersCount()+1);
					$manager->persist($newWorker);
				}
    	}
    	for ($i=0; $i < 2; $i++){
				$newAgent = new Agent();
				$newAgent->setName('AgentNieAktywny '.$i);
				$newAgent->setStatus(false);
				$newAgent->setWorkersCount(0);
				$manager->persist($newAgent);

				for ($a=0; $a < rand(1,5); $a++) {
					$newWorker = new Worker();
					$newWorker->setName('Pracownik ' . $a);
					$newWorker->setStatus(false);
					$newWorker->setAgent($newAgent);
					$newWorker->setAgentIp(ip2long('127.0.0.1'));
					$newAgent->setWorkersCount($newAgent->getWorkersCount()+1);
					$manager->persist($newWorker);
				}
    	}

			$manager->flush();
    }
}
