<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Entity\Worker;
use App\Form\AgentType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AgentsController extends AbstractController
{
    /**
     * @Route("/", name="agents")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
			$query = $this->getDoctrine()->getRepository(Agent::class)->findAllQuery($filterData ?? null);
			$agents = $paginator->paginate($query, $request->query->getInt('page', 1), 5);
			$agentsForChart = $this->getDoctrine()->getRepository(Worker::class)->findForChart();

			return $this->render('agents/index.html.twig', compact('agents','agentsForChart'));
    }

    /**
     * @Route("/agent/create/", name="agents_create")
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
			$newAgent = new Agent();
			$form = $this->createForm(AgentType::class, $newAgent);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$newAgent = $form->getData();
				$newAgent->setWorkersCount(0);
				$em->persist($newAgent);
				$em->flush();

				$this->addFlash('success', 'Agent został dodany');
				return $this->redirectToRoute('agents');
			}

			return $this->render('agents/create.html.twig', [
				'form' => $form->createView()
			]);
    }

    /**
     * @Route("/agent/update/{agentId}", name="agents_update")
     */
    public function update(Request $request, EntityManagerInterface $em, Agent $agentId)
    {
			$form = $this->createForm(AgentType::class, $agentId);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$newAgent = $form->getData();
				$em->persist($newAgent);
				$em->flush();

				$this->addFlash('success', 'Agent został poprawiony');
				return $this->redirectToRoute('agents');
			}

			return $this->render('agents/create.html.twig', [
				'form' => $form->createView(),
				'agent' => $agentId
			]);
    }

    /**
     * @Route("/agent/delete/{agentId}", name="agents_delete")
     */
    public function delete(EntityManagerInterface $entityManagerInterface, Agent $agentId)
    {
			$repository = $entityManagerInterface->getRepository(Agent::class);
			$agent = $repository->find($agentId);
			$entityManagerInterface->remove($agent);
			$entityManagerInterface->flush();
			$this->addFlash('danger', 'Agent ' . $agent->getName() . ' usunięty');

			return $this->redirectToRoute('agents');

		}

		private function getData(): array
		{
			$list = [];
			$agents = $this->getDoctrine()->getRepository(Agent::class)->findAll();
			$workers = $this->getDoctrine()->getRepository(Worker::class)->findAll();
			$lp = 1;
			foreach ($agents as $agent) {
				$list[] = [
					$lp,
					$agent->getId(),
					$agent->getName(),
					$agent->getStatus(),
					$agent->getWorkersCount()
				];
				$lp++;
			}
			foreach ($workers as $worker) {
				$list[] = [
					$lp,
					$worker->getAgent()->getId(),
					'',
					'',
					'',
					$worker->getId(),
					$worker->getName(),
					$worker->getStatus(),
					$worker->getCreatedAt(),
					long2ip($worker->getAgentIp())
				];
				$lp++;
			}
			return $list;
		}

		/**
		 * @Route("/export/",  name="export")
		 */
		public function export()
		{
			$spreadsheet = new Spreadsheet();

			$sheet = $spreadsheet->getActiveSheet();

			$sheet->setTitle('Agenci i pracownicy');

			$sheet->getCell('A1')->setValue('Liczba porządkowa');
			$sheet->getCell('B1')->setValue('ID agencji');
			$sheet->getCell('C1')->setValue('Nazwa');
			$sheet->getCell('D1')->setValue('Status');
			$sheet->getCell('E1')->setValue('Ilość pracowników');
			$sheet->getCell('F1')->setValue('ID pracownika');
			$sheet->getCell('G1')->setValue('Imię i nazwisko');
			$sheet->getCell('H1')->setValue('Status');
			$sheet->getCell('I1')->setValue('Data i czas dodania');
			$sheet->getCell('J1')->setValue('IP osoby dodającej');

			// Increase row cursor after header write
			$sheet->fromArray($this->getData(),null, 'A2', true);


			$writer = new Xlsx($spreadsheet);

			try {
				$writer->save('agentlist.xlsx');
				$this->addFlash('success', 'Export wykonany');
			}catch (\Exception $e){
				$this->addFlash('danger', 'Wystąpił błąd!');
			}
			return $this->redirectToRoute('agents');
		}
}
