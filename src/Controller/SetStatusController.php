<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Entity\Worker;
use App\Form\AgentType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SetStatusController extends AbstractController
{
    /**
     * @Route("/set-agent-status/{id}", name="set_agent_status")
     */
    public function agentStatus(Agent $id, EntityManagerInterface $em)
    {
			$agent = $id;
			if($agent->getStatus() == false){
				$agent->setStatus(true);
				if(count($agent->getWorkers()) != 0){
					foreach($agent->getWorkers() as $worker){
						$worker->setStatus(true);
						$em->persist($worker);
					}
				}
			}else{
				$agent->setStatus(false);
				if(count($agent->getWorkers()) != 0){
					foreach($agent->getWorkers() as $worker){
						$worker->setStatus(false);
						$em->persist($worker);
					}
				}
			}

			$em->persist($agent);
			$em->flush();

			return $this->redirectToRoute('agents');
    }

    /**
     * @Route("/set-worker-status/{id}", name="set_worker_status")
     */
    public function workerStatus(Worker $id, EntityManagerInterface $em)
    {
			$worker = $id;
			if($worker->getStatus() == false){
				$worker->setStatus(true);
			}else{
				$worker->setStatus(false);
			}
			$em->persist($worker);
			$em->flush();

			return $this->redirectToRoute('workers', ['agentId' => $worker->getAgent()->getId()]);
    }

}
