<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Entity\Worker;
use App\Form\WorkerType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WorkersController extends AbstractController
{
    /**
     * @Route("/worker/{agentId}", name="workers")
     */
    public function index(Request $request, PaginatorInterface $paginator, Agent $agentId)
    {
			$query = $this->getDoctrine()->getRepository(Worker::class)->findAllQuery($agentId);
			$workers = $paginator->paginate($query, $request->query->getInt('page', 1), 5);

			return $this->render('workers/index.html.twig', [
				'workers' => $workers,
				'agent' => $agentId
			]);
    }

    /**
     * @Route("/worker/{agentId}/create/", name="workers_create")
     */
    public function create(Request $request, EntityManagerInterface $em, Agent $agentId)
    {
			$newWorker = new Worker();
			$form = $this->createForm(WorkerType::class, $newWorker);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$newWorker = $form->getData();
				$newWorker->setAgent($agentId);
				$newWorker->setAgentIp(ip2long($this->getIp()));
				$agentId->setWorkersCount($agentId->getWorkersCount()+1);

				$em->persist($newWorker);
				$em->flush();

				$this->addFlash('success', 'Pracownik został dodany');
				return $this->redirectToRoute('workers', ['agentId' => $agentId->getId()]);
			}

			return $this->render('workers/create.html.twig', [
				'form' => $form->createView(),
				'agent' => $agentId
			]);
    }

    /**
     * @Route("/worker/update/{workerId}", name="workers_update")
     */
    public function update(Request $request, EntityManagerInterface $em, Worker $workerId)
    {
			$form = $this->createForm(WorkerType::class, $workerId);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$newWorker = $form->getData();
				$em->persist($newWorker);
				$em->flush();

				$this->addFlash('success', 'Pracownik został poprawiony');
				return $this->redirectToRoute('workers', ['agentId' => $workerId->getAgent()->getId()]);
			}

			return $this->render('workers/create.html.twig', [
				'form' => $form->createView(),
				'worker' => $workerId,
				'agent' => $workerId->getAgent()
			]);
    }

    /**
     * @Route("/worker/delete/{workerId}", name="workers_delete")
     */
    public function delete(EntityManagerInterface $entityManagerInterface, Worker $workerId)
    {
			$repository = $entityManagerInterface->getRepository(Worker::class);
			$worker = $repository->find($workerId);
			$entityManagerInterface->remove($worker);
			$entityManagerInterface->flush();
			$this->addFlash('danger', 'Pracownik ' . $worker->getName() . ' usunięty');
			$agent = $workerId->getAgent();
			$agent->setWorkersCount($agent->getWorkersCount()-1);
			$entityManagerInterface->persist($agent);
			$entityManagerInterface->flush();

			return $this->redirectToRoute('workers', ['agentId' => $agent->getId()]);

		}

		public function getIp(){
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}
}
