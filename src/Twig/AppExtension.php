<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
  public function __construct()
  {
  }

  public function getFunctions()
  {
    return [
      new TwigFunction('long2ip', [$this, 'long2ip']),
    ];
  }
	public function long2ip($ip)
	{
				return long2ip($ip);
	}
}