<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211209021317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE agents (id INT AUTO_INCREMENT NOT NULL, `name` VARCHAR(255) NOT NULL, `status` TINYINT(1) NOT NULL, workers_count INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workers (id INT AUTO_INCREMENT NOT NULL, agents_id INT NOT NULL, `name` VARCHAR(255) NOT NULL, `status` TINYINT(1) NOT NULL, agent_ip INT NOT NULL, created_at DATETIME DEFAULT NULL, INDEX fk_workers_agents_idx (agents_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workers ADD CONSTRAINT FK_B82D7DC0709770DC FOREIGN KEY (agents_id) REFERENCES agents (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE workers DROP FOREIGN KEY FK_B82D7DC0709770DC');
        $this->addSql('DROP TABLE agents');
        $this->addSql('DROP TABLE workers');
    }
}
