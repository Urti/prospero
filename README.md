Strona internetowa, która zawiera tabelę „Lista agentów”
========================
Zadanie rekrutacyjne.

Symfony 5.4
## Instalacja
1. > composer install
2. > php bin/console doctrine:database:create
3. > php bin/console doctrine:migration:migrate
4. > php bin/console doctrine:fixtures:load
   
### Uruchomienie symfony
> symfony serve
